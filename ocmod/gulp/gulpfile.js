'use strict';

var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    sourcemaps = require('gulp-sourcemaps');

sass.compiler = require('node-sass');


gulp.task('scss', function () {
  return gulp.src('./src/scss/*.scss')
    .pipe(sourcemaps.init())
    .pipe(sass({outputStyle: 'compressed'}).on('error', sass.logError))
    .pipe(autoprefixer())
    .pipe(sourcemaps.write())
    .pipe(gulp.dest('./../../catalog/view/theme/anvi_updater/stylesheet'));
});

gulp.task('watch', function() {
  gulp.watch('./src/scss/**/*.scss', gulp.series('scss'));
});