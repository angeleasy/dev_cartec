<div class="main-advantage-row cat-wall-row">
	<div class="oct-carousel-header"><?php echo $heading_title; ?></div>
	<div class="cat-wall-box">
	  <?php foreach ($categories as $category) { ?>
	  		<div class="oct-category-item-box">
	  			<?php if ($category['thumb']) { ?>
	  			<div class="main-advantage-item-icon oct-category-item-icon col-md-4 col-sm-4 col-xs-4">
	  				<a href="<?php echo $category['href']; ?>"><img class="img-responsive" src="<?php echo $category['thumb']; ?>" alt="<?php echo $category['name']; ?>" title="<?php echo $category['name']; ?>" /></a>
	  			</div>
			    <?php } ?>
			    <div class="main-advantage-item-text oct-category-item-text col-md-8 col-sm-8 col-xs-8">
			    		<a href="<?php echo $category['href']; ?>" class="oct-category-item-header"><?php echo $category['name']; ?></a>
			    		<?php if ($category['children']) { ?>
			    		<ul class="list-unstyled">
				      <?php $countstop = 1; foreach ($category['children'] as $child) { $countstop++; ?>
				        <li><a href="<?php echo $child['href']; ?>"><?php echo $child['name']; ?></a></li>
				        <?php if ($countstop > $limit) { ?>
				        <li class="oct-category-see-more"><a href="<?php echo $category['href']; ?>" ><?php echo $text_see_more; ?></a></li>
				        <?php break; } ?>
				      <?php } ?>
				    </ul>
				    <?php } ?>
			    </div>
			</div>
	  <?php } ?>
	</div>
</div>
