<?php
class ControllerCommonHeader extends Controller {
	public function index() {

        // oct_product_preorder start
        $data['oct_product_preorder_data'] = $this->config->get('oct_product_preorder_data');
        // oct_product_preorder end
      

        // oct_popup_call_phone start
        $data['oct_popup_call_phone_data'] = $this->config->get('oct_popup_call_phone_data');
        $data['popup_call_phone_text'] = $this->language->load('extension/module/oct_popup_call_phone');
        // oct_popup_call_phone end
      
		// Analytics
		$this->load->model('extension/extension');

		$data['analytics'] = array();

		$analytics = $this->model_extension_extension->getExtensions('analytics');

		foreach ($analytics as $analytic) {

                if (!$this->config->get($analytic['code'] . '_position')) {
          
			if ($this->config->get($analytic['code'] . '_status')) {
				$data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));

              }
        
			}
		}

		if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
			$this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
		}


		if ($this->config->get('oct_analytics_google_status') && $this->config->get('oct_analytics_google_webmaster_code')) {
			$data['oct_analytics_google_webmaster_code'] = html_entity_decode($this->config->get('oct_analytics_google_webmaster_code'), ENT_QUOTES, 'UTF-8');
		}

		if ($this->config->get('oct_analytics_yandex_status') && $this->config->get('oct_analytics_yandex_webmaster_code')) {
			$data['oct_analytics_yandex_webmaster_code'] = html_entity_decode($this->config->get('oct_analytics_yandex_webmaster_code'), ENT_QUOTES, 'UTF-8');
		}
		

			$data['oct_megamenu_data'] = $this->config->get('oct_megamenu_data');
	        $oct_megamenu_data = $this->config->get('oct_megamenu_data');

	        $data['oct_megamenu'] = '';
			
		$data['title'] = $this->document->getTitle();

		$data['base'] = $server;
		$data['description'] = $this->document->getDescription();
		$data['keywords'] = $this->document->getKeywords();
		$data['links'] = $this->document->getLinks();
		
			
		
			

			$data['robots'] = $this->document->getRobots();
			
		$data['lang'] = $this->language->get('code');
		$data['direction'] = $this->language->get('direction');

		$data['name'] = $this->config->get('config_name');

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->language('common/header');
		$data['og_url'] = (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1')) ? HTTPS_SERVER : HTTP_SERVER) . substr($this->request->server['REQUEST_URI'], 1, (strlen($this->request->server['REQUEST_URI'])-1));
		$data['og_image'] = $this->document->getOgImage();


        // oct_blog start
        if (isset($this->request->get['oct_blog_article_id'])) {
          $oct_blog_article_id = (int)$this->request->get['oct_blog_article_id'];
        } else {
          $oct_blog_article_id = 0;
        }

        $this->load->model('octemplates/blog_article');
        $article_info = $this->model_octemplates_blog_article->getArticle($oct_blog_article_id);

        $data['og_meta_description'] = "";

        if ($article_info) {
          $this->load->model('tool/image');
          $data['og_image'] = $this->model_tool_image->resize($article_info['image'], 500, 500);
          $data['og_meta_description'] = utf8_substr(strip_tags(html_entity_decode($article_info['meta_description'], ENT_QUOTES, 'UTF-8')), 0, 250);
        }
        // oct_blog end
        
		$data['text_home'] = $this->language->get('text_home');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');

			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}

		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');

			$data['text_page'] = $this->language->get('text_page');
			
		$data['text_page'] = $this->language->get('text_page');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all'] = $this->language->get('text_all');

		$data['home'] = $this->url->link('common/home');
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		$data['contact'] = $this->url->link('information/contact');
		$data['telephone'] = $this->config->get('config_telephone');

			$this->load->language('octemplates/oct_techstore');
			$data['oct_techstore_news'] = $this->language->get('oct_techstore_news');
			$data['oct_techstore_contact'] = $this->language->get('oct_techstore_contact');
			$data['oct_techstore_clock'] = $this->language->get('oct_techstore_clock');
			$data['oct_techstore_client_center'] = $this->language->get('oct_techstore_client_center');
			$data['oct_techstore_see_more'] = $this->language->get('oct_techstore_see_more');
			$data['oct_techstore_mmenu'] = $this->language->get('oct_techstore_mmenu');
			$data['oct_techstore_minfo'] = $this->language->get('oct_techstore_minfo');
			$data['oct_techstore_msearch'] = $this->language->get('oct_techstore_msearch');
			$data['oct_techstore_msearchb'] = $this->language->get('oct_techstore_msearchb');
			$data['oct_techstore_data'] = $oct_data = $this->config->get('oct_techstore_data');
			$data['oct_techstore_status'] = $this->config->get('oct_techstore_status');
			$data['link_cart'] = $this->url->link('checkout/cart');
			$data['link_wishlist'] = $this->url->link('account/wishlist', '', true);
			$data['link_compare'] = $this->url->link('product/compare');
			$data['link_login'] = $this->url->link('account/login', '', true);

			$data['text_news'] = $this->language->get('oct_techstore_news');
			$data['text_contact'] = $this->language->get('oct_techstore_contact');
			$data['text_clock'] = $this->language->get('oct_techstore_clock');
			$data['text_client_center'] = $this->language->get('oct_techstore_client_center');
			$data['text_see_more'] = $this->language->get('oct_techstore_see_more');
			$data['oct_techstore_news'] = $this->url->link('octemplates/blog_articles');
			// Вывод ссылок на статьи
			$data['oct_techstore_header_information_links'] = array();
			$data['$oct_techstore_header_customer_links'] = array();

			if (isset($oct_data['header_information_links'])) {
				$this->load->model('catalog/information');
				
				foreach ($oct_data['header_information_links'] as $result) {
					$information_info = $this->model_catalog_information->getInformation((int)$result);
					
					if ($information_info) {
						$data['oct_techstore_header_information_links'][] = array(
							'title' => $information_info['title'],
							'sort_order' => $information_info['sort_order'],
							'href' => $this->url->link('information/information', 'information_id=' . $information_info['information_id'])
						);
					}
				}
				
				$sort_information = array();
				foreach ($data['oct_techstore_header_information_links'] as $key => $row) {
					$sort_information[$key] = $row['sort_order'];
				}
				array_multisort($sort_information, SORT_ASC, $data['oct_techstore_header_information_links']);
				
			}
			
			if (isset($oct_data['header_custom_links']) && !empty($oct_data['header_custom_links'])) {
				foreach ($oct_data['header_custom_links'] as $header_custom_link) {
					if (isset($header_custom_link['name'][(int)$this->config->get('config_language_id')]) && !empty($header_custom_link['name'][(int)$this->config->get('config_language_id')])) {
						$data['oct_techstore_header_customer_links'][] = array(
							'title' => $header_custom_link['name'][(int)$this->config->get('config_language_id')],
							'href' => $header_custom_link['link']
						);
					}
				}
			}

			$data['styles'] = array();
			$data['styless'] = array();
			$data['scripts'] = array();
			$data['scriptes'] = array();

			$this->document->addScript('catalog/view/javascript/jquery/jquery-2.1.1.min.js', 'header', 'octemplates');
            $this->document->addScript('catalog/view/javascript/bootstrap/js/bootstrap.min.js', 'header', 'octemplates');
            $this->document->addScript('catalog/view/javascript/jquery/owl-carousel/owl.carousel.min.js', 'header', 'octemplates');
            $this->document->addScript('catalog/view/javascript/jquery/magnific/jquery.magnific-popup.min.js', 'header', 'octemplates');
            $this->document->addScript('catalog/view/theme/oct_techstore/js/barrating.js', 'header', 'octemplates');
            $this->document->addScript('catalog/view/javascript/octemplates/tippy/tippy.min.js', 'header', 'octemplates');

            //$this->document->addScript('catalog/view/theme/oct_techstore/js/main.js', 'header', 'octemplates');
            //$this->document->addScript('catalog/view/theme/oct_techstore/js/common.js', 'header', 'octemplates');
            //$this->document->addScript('catalog/view/theme/oct_techstore/js/toast/jquery.toast.js', 'header', 'octemplates');
            //$this->document->addScript('catalog/view/theme/oct_techstore/js/flexmenu.min.js', 'header', 'octemplates');
            //$this->document->addScript('catalog/view/theme/oct_techstore/js/flipclock.js', 'header', 'octemplates');


			$oct_styles = array(
				'catalog/view/javascript/bootstrap/css/bootstrap.min.css',
				'catalog/view/theme/oct_techstore/stylesheet/font-awesome/css/font-awesome.min.css',
				'catalog/view/theme/oct_techstore/stylesheet/fonts.css',
				'catalog/view/theme/oct_techstore/stylesheet/stylesheet.css',
				'catalog/view/theme/oct_techstore/stylesheet/responsive.css',
				'catalog/view/javascript/jquery/owl-carousel/owl.carousel.css',
				'catalog/view/theme/oct_techstore/stylesheet/allstyles.css',
				'catalog/view/theme/oct_techstore/stylesheet/dynamic_stylesheet.css',
			);

			$folder = $_SERVER['DOCUMENT_ROOT'].str_replace('index.php','',$_SERVER['SCRIPT_NAME']);
			$scripts = array();

			foreach ($this->document->getScripts('header', 'octemplates') as $script) {
				$del_version = explode("?", $script);

				if (file_exists($folder.$del_version[0])) {
					$scripts[$script] = ltrim($del_version[0], "/");
				} else {
					$data['scriptes'][$script] = $script;
				}
			}

			foreach ($this->document->getStyles() as $style) {
				$del_version = explode("?", $style['href']);

				if (file_exists($folder.$del_version[0])) {
					if (!in_array(ltrim($del_version[0], "/"), $oct_styles)) {
						$oct_styles[] = ltrim($del_version[0], "/");
					}
				} else {
					$data['styless'][$style['href']] = array(
						'href'  => $style['href'],
						'rel'   => $style['rel'],
						'media' => $style['media']
					);
				}
			}

			if ($data['oct_techstore_data']['enable_minify'] == 'on') {
				require_once $_SERVER['DOCUMENT_ROOT'] . '/min/utils.php';

				$data['scripts'][0] = html_entity_decode(Minify_getUri($scripts, array('farExpires' => false, 'rewriteWorks' => false)), ENT_QUOTES, 'UTF-8');

				$data['styles'][0] = html_entity_decode(Minify_getUri($oct_styles, array('farExpires' => false, 'rewriteWorks' => false)), ENT_QUOTES, 'UTF-8');
			} else {
				$data['scripts'] = $scripts;
				$data['styles'] = $oct_styles;
			}

			// Свой CSS и Javascript код
			//$data['oct_techstore_customcss'] = html_entity_decode($oct_data['customcss'], ENT_QUOTES, 'UTF-8');
			$data['oct_techstore_customjavascrip'] = html_entity_decode($oct_data['customjavascrip'], ENT_QUOTES, 'UTF-8');
			$oct_techstore_cont_clock = $oct_data['cont_clock'];

			if (isset($oct_techstore_cont_clock[$this->session->data['language']]) && !empty($oct_techstore_cont_clock[$this->session->data['language']])) {
				$data['oct_techstore_cont_clock'] = array_values(array_filter(explode(PHP_EOL, $oct_techstore_cont_clock[$this->session->data['language']])));
			} else {
				$data['oct_techstore_cont_clock'] = false;
			}

			if (isset($oct_data['cont_phones']) && !empty($oct_data['cont_phones'])) {
				$data['oct_techstore_cont_phones'] = array_values(array_filter(explode(PHP_EOL, $oct_data['cont_phones'])));
			} else {
				$data['oct_techstore_cont_phones'] = false;
			}
			

		// Menu
		$this->load->model('catalog/category');

		$this->load->model('catalog/product');

		$data['categories'] = array();

			// start: oct_megamenu
	        if (isset($oct_megamenu_data['status']) && $oct_megamenu_data['status'] == 1) {
	            $data['oct_megamenu'] = $this->load->controller('extension/module/oct_megamenu');
	        } else {
			

		$categories = $this->model_catalog_category->getCategories(0);

		foreach ($categories as $category) {
			if ($category['top']) {
				// Level 2
				$children_data = array();

				$children = $this->model_catalog_category->getCategories($category['category_id']);

				foreach ($children as $child) {
					$filter_data = array(
						'filter_category_id'  => $child['category_id'],
						'filter_sub_category' => true
					);


			// Level 3
			$children_data_2 = array();

			$children_2 = $this->model_catalog_category->getCategories($category['category_id']);

			foreach ($children_2 as $child_2) {
				$filter_data2 = array(
					'filter_category_id'  => $child_2['category_id'],
					'filter_sub_category' => true
				);

				$children_data_2[] = array(
					'name'  => $child_2['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data2) . ')' : ''),
					'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'] . '_' . $child_2['category_id'])
				);
			}
			
					$children_data[] = array(

			'children' => $children_data_2,
			
						'name'  => $child['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
						'href'  => $this->url->link('product/category', 'path=' . $category['category_id'] . '_' . $child['category_id'])
					);
				}

				// Level 1
				$data['categories'][] = array(
					'name'     => $category['name'],
					'children' => $children_data,
					'column'   => $category['column'] ? $category['column'] : 1,
					'href'     => $this->url->link('product/category', 'path=' . $category['category_id'])
				);
			}
		}


			}// end: oct_megamenu
			
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['cart'] = $this->load->controller('common/cart');

		// For page specific css
		if (isset($this->request->get['route'])) {
			if (isset($this->request->get['product_id'])) {
				$class = '-' . $this->request->get['product_id'];
			} elseif (isset($this->request->get['path'])) {
				$class = '-' . $this->request->get['path'];
			} elseif (isset($this->request->get['manufacturer_id'])) {
				$class = '-' . $this->request->get['manufacturer_id'];
			} elseif (isset($this->request->get['information_id'])) {
				$class = '-' . $this->request->get['information_id'];
			} else {
				$class = '';
			}

			$data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
		} else {
			$data['class'] = 'common-home';
		}

		return $this->load->view('common/header', $data);
	}
}
